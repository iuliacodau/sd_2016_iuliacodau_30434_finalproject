package com.cabcaller.web.authentication;

import com.cabcaller.web.dao.UserDAOImplementation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Iulia on 26.05.2016.
 */
@Service("userDetailsService")
public class MyCustomAuthenticationProvider implements UserDetailsService {

    @Autowired
    private UserDAOImplementation userDAO=new UserDAOImplementation();

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.cabcaller.web.model.User user=userDAO.findByUsername(username);
        List<GrantedAuthority> authorities=createUserAuthority(user.getType());
        return createUserForAuthentication(user, authorities);
    }

    private org.springframework.security.core.userdetails.User createUserForAuthentication(com.cabcaller.web.model.User user, List<GrantedAuthority> authorities) {
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), true, true, true, true, authorities );
    }

    private List<GrantedAuthority> createUserAuthority(String userRole) {
        Set<GrantedAuthority> setAuthorities = new HashSet<GrantedAuthority>();
        setAuthorities.add(new SimpleGrantedAuthority(userRole));
        List<GrantedAuthority> Result=new ArrayList<GrantedAuthority>(setAuthorities);
        return Result;

    }
}

