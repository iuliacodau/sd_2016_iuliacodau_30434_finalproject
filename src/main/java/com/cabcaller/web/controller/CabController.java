package com.cabcaller.web.controller;

import com.cabcaller.web.dao.CabDAO;
import com.cabcaller.web.dao.UserDAO;
import com.cabcaller.web.model.Cab;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Controller
@EnableWebMvc
@RequestMapping("/rest")
public class CabController {


    @Autowired
    CabDAO cabDAO;

    //Creating a new user
    @RequestMapping(value = "/cab", method = RequestMethod.POST, consumes = "application/JSON", produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Cab> createNewCab(@RequestBody Cab cab) {
        try {
            cabDAO.create(cab);
        } catch (Exception e) {
            return new ResponseEntity<Cab>(HttpStatus.NOT_FOUND);
        }
        HttpHeaders hdr=new HttpHeaders();
        hdr.add("Location", "/rest/cabs/" + cab.getId());
        return new ResponseEntity<Cab>(cab, hdr, HttpStatus.CREATED);
    }

    //Select user according to its id (in order to update or delete later
    @RequestMapping(value = "/cabs/{id}", method = RequestMethod.GET, produces = "application/JSON")
    @ResponseBody
    public ResponseEntity<Cab> selectCab(@PathVariable String id) {
        try {
            Cab cab = cabDAO.findById(id);
            return new ResponseEntity<Cab>(cab, HttpStatus.FOUND);
        } catch(Exception e) {
            return new ResponseEntity<Cab>(HttpStatus.NOT_FOUND);
        }
    }

    //Get all the users from the database
    @RequestMapping (value = "/cabs", method = RequestMethod.GET, produces="application/JSON")
    @ResponseBody
    public ResponseEntity<List<Cab>> showCabs(Model model) {
        List<Cab> cabsList;
        try {
            model.addAttribute("cab", new Cab());
            model.addAttribute("cabsList", cabDAO.find());
            cabsList=cabDAO.find();
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } return new ResponseEntity<>(cabsList, HttpStatus.OK);
    }

    //Updating a user
    @RequestMapping (value = "/cabs/{id}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Cab> updateCab(@PathVariable("id") String id, @RequestBody Cab cab) {
        try {
            if (cabDAO.findById(id).getId().equals(id)) {
                cab.setId(id);
                cabDAO.update(cab);
            }
        } catch (Exception e) {
            return new ResponseEntity<Cab>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Cab>(cab, HttpStatus.OK);
    }

    //Deleting a user
    @RequestMapping (value = "/cabs/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Cab> deleteCab(@PathVariable("id") String id) {
        try {
            cabDAO.delete(id);
        } catch (Exception e) {
            return new ResponseEntity<Cab>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<Cab>(HttpStatus.OK);
    }
}
