package com.cabcaller.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by Iulia on 26.05.2016.
 */

@Controller
@EnableWebMvc
public class MainController {

    @RequestMapping(value = "/{path}", method = RequestMethod.GET)
    public String getPage(@PathVariable("path") String path) {
        return path;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getPathPage() {
        return "login";
    }
}
