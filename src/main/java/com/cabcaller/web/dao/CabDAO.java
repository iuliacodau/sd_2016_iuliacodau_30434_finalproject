package com.cabcaller.web.dao;

import com.cabcaller.web.model.Cab;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Component
public interface CabDAO {

    public void create(Cab cab);
    public void update(Cab cab);
    public void delete(String id);
    public List<Cab> find();
    public Cab findById(String id);
    public Cab findByRating(String id);
}
