package com.cabcaller.web.dao;

import com.cabcaller.web.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Iulia on 26.05.2016.
 */

@Repository(value = "ORDER")
public class OrderDAOImplementation implements OrderDAO {

    @Autowired
    MongoTemplate mongoTemplate;

    @Override
    public void create(Order order) {
        mongoTemplate.insert(order);
    }

    @Override
    public void update(Order order) {
        mongoTemplate.save(order);
    }

    @Override
    public void delete(String id) {
        mongoTemplate.remove(Query.query(Criteria.where("id").is(id)), Order.class);
    }

    @Override
    public List<Order> find() {
        return mongoTemplate.findAll(Order.class);
    }

    @Override
    public Order findById(String id) {
        Order order=new Order();
        List<Order> orderListe=mongoTemplate.find(Query.query(Criteria.where("id").is(id)), Order.class);
        if (!orderListe.isEmpty()) {
            order=orderListe.get(0);
        }
        return order;
    }

    public Order findByCab(String cab) {
        Order order=new Order();
        List<Order> orderListe=mongoTemplate.find(Query.query(Criteria.where("cabId").is(cab)), Order.class);
        if (!orderListe.isEmpty()) {
            order=orderListe.get(0);
        }
        return order;
    }
}
