'use strict';

App.controller('ConsultationController', ['$scope', 'ConsultationService', function($scope, ConsultationService) {
    var self = this;
    self.consultation={id:null,patientId:'',doctorId:'',dateOfConsultation:''};
    self.consultations=[];

    self.fetchAllConsultations = function(){
        ConsultationService.fetchAllConsultations()
            .then(
                function(d) {
                    self.consultations = d;
                },
                function(errResponse){
                    console.error('Error while fetching consultations');
                }
            );
    };

    self.createConsultation = function(consultation){
        ConsultationService.createConsultation(consultation)
            .then(
                self.fetchAllConsultations,
                function(errResponse){
                    console.error('Error while creating Consultation.');
                }
            );
    };

    self.updateConsultation = function(consultation, id){
        ConsultationService.updateConsultation(consultation, id)
            .then(
                self.fetchAllConsultations,
                function(errResponse){
                    console.error('Error while updating Consultation.');
                }
            );
    };

    self.deleteConsultation = function(id){
        ConsultationService.deleteConsultation(id)
            .then(
                self.fetchAllConsultations,
                function(errResponse){
                    console.error('Error while deleting Consultation.');
                }
            );
    };

    self.fetchAllConsultations();

    self.submit = function() {
        if(self.consultation.id===null){
            console.log('Saving New Consultation', self.consultation);
            self.createConsultation(self.consultation);
        }else{
            self.updateConsultation(self.consultation, self.consultation.id);
            console.log('Consultation updated with id ', self.consultation.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.consultations.length; i++){
            if(self.consultations[i].id === id) {
                self.consultation = angular.copy(self.consultations[i]);
                break;
            }
        }
    };

    self.remove = function(id){
        console.log('id to be deleted', id);
        if(self.consultation.id === id) {//clean form if the user to be deleted is shown there.
            self.reset();
        }
        self.deleteConsultation(id);
    };


    self.reset = function(){
        self.consultation={id:null,patientId:'',doctorId:'',dateOfConsultation:''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);