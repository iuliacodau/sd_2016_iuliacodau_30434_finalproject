'use strict';

App.controller('OrderController', ['$scope', 'OrderService', 'CabService', function($scope, OrderService, CabService) {
    var self = this;
    self.order={id:null,cabId:'',startLocation:'',endLocation:''};
    self.orders=[];
    self.cab={id:null, driver:'', rating:'',location:''};
    self.cabs=[];

    self.fetchAllOrders = function(){
        OrderService.fetchAllOrders()
            .then(
                function(d) {
                    self.orders = d;
                },
                function(errResponse){
                    console.error('Error while fetching orders');
                }
            );
    };

    self.fetchAllCabs = function(){
        CabService.fetchAllCabs()
            .then(
                function(d) {
                    self.cabs = d;
                },
                function(errResponse){
                    console.error('Error while fetching orders');
                }
            );
    };
    
    self.createOrder = function(id,order){
        order.cabId=id;
        OrderService.createOrder(order)
            .then(
                self.fetchAllOrders,
                function(errResponse){
                    console.error('Error while creating order.');
                }
            );
    };

    self.updateOrder = function(order, id){
        OderService.updateOrder(order, id)
            .then(
                self.fetchAllOrders,
                function(errResponse){
                    console.error('Error while updating order.');
                }
            );
    };

    self.deleteOrder = function(id){
        OrderService.deleteOrder(id)
            .then(
                self.fetchAllOrders,
                function(errResponse){
                    console.error('Error while deleting order.');
                }
            );
    };

    self.fetchAllOrders();
    self.fetchAllCabs();

    self.submit = function() {
        if(self.order.id===null){
            console.log('Saving New Order', self.order);
            self.createOrder(self.order);
        }else{
            self.updateOrder(self.order, self.order.id);
            console.log('Order updated with id ', self.order.id);
        }
        self.reset();
    };

    self.edit = function(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.orders.length; i++){
            if(self.orders[i].id === id) {
                self.order = angular.copy(self.orders[i]);
                break;
            }
        }
    };

    self.remove = function(id, cabId){
        console.log('id to be deleted', id);
        if((self.order.id === id) &&(self.cab.id === cabId)) {//clean form if the user to be deleted is shown there.
            self.reset();
        }
        self.deleteOrder(id);
        self.updateCab(self.cab, cabId);
    };


    self.reset = function(){
        self.order={id:null,cabId:'',startLocation:'',endLocation:''};
        $scope.myForm.$setPristine(); //reset Form
    };

}]);