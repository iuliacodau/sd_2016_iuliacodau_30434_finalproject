'use strict';

App.factory('CabService', ['$http', '$q', function($http, $q){

    return{

        fetchAllCabs: function() {
            return $http.get('http://localhost:8080/rest/cabs/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching cabs');
                        return $q.reject(errResponse);
                    }
                );
        },

        createCab: function(cab){
            return $http.post('http://localhost:8080/rest/cab/', cab)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating cab');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateCab: function(cab, id){
            return $http.put('http://localhost:8080/rest/cabs/'+id, cab)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating cab');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateCabLocation: function(cabId, cabLocation){
            return $http.put('http://localhost:8080/rest/cabsLocation/'+cabId, cabLocation)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating cab');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteCab: function(id){
            return $http.delete('http://localhost:8080/rest/cabs/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting cabs');
                        return $q.reject(errResponse);
                    }
                );
        }
    };
}]);