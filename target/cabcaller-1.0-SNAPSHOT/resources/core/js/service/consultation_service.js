'use strict';

App.factory('ConsultationService', ['$http', '$q', function($http, $q){

    return{

        fetchAllConsultations: function() {
            return $http.get('http://localhost:8080/rest/consultations/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching patients');
                        return $q.reject(errResponse);
                    }
                );
        },

        createConsultation: function(consultation){
            return $http.post('http://localhost:8080/rest/consultation/', consultation)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating consultation');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateConsultation: function(consultation, id){
            return $http.put('http://localhost:8080/rest/consultations/'+id, consultation)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating consultation');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteConsultation: function(id){
            return $http.delete('http://localhost:8080/rest/consultations/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting consultation');
                        return $q.reject(errResponse);
                    }
                );
        }
    };
}]);