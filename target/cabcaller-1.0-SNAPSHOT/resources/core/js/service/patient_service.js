'use strict';

App.factory('PatientService', ['$http', '$q', function($http, $q){

    return{

        fetchAllPatients: function() {
            return $http.get('http://localhost:8080/rest/patients/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching patients');
                        return $q.reject(errResponse);
                    }
                );
        },

        createPatient: function(patient){
            return $http.post('http://localhost:8080/rest/patient/', patient)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating patient');
                        return $q.reject(errResponse);
                    }
                );
        },

        updatePatient: function(patient, id){
            return $http.put('http://localhost:8080/rest/patients/'+id, patient)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating patient');
                        return $q.reject(errResponse);
                    }
                );
        },

        deletePatient: function(id){
            return $http.delete('http://localhost:8080/rest/patients/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting patients');
                        return $q.reject(errResponse);
                    }
                );
        }
    };
}]);