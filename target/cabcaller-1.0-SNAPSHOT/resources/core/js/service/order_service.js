'use strict';

App.factory('OrderService', ['$http', '$q', function($http, $q){

    return{

        fetchAllOrders: function() {
            return $http.get('http://localhost:8080/rest/orders/')
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while fetching orders');
                        return $q.reject(errResponse);
                    }
                );
        },

        createOrder: function(order){
            return $http.post('http://localhost:8080/rest/order/', order)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while creating order');
                        return $q.reject(errResponse);
                    }
                );
        },

        updateOrder: function(order, id){
            return $http.put('http://localhost:8080/rest/orders/'+id, order)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while updating order');
                        return $q.reject(errResponse);
                    }
                );
        },

        deleteOrder: function(id){
            return $http.delete('http://localhost:8080/rest/orders/'+id)
                .then(
                    function(response){
                        return response.data;
                    },
                    function(errResponse){
                        console.error('Error while deleting order');
                        return $q.reject(errResponse);
                    }
                );
        }
    };
}]);