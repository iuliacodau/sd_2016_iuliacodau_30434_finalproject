<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html ng-app="myApp">
<head>
    <title>Cabs and Drivers</title>
    <style>
        .username.ng-valid {
            background-color: lightgreen;
        }
        .username.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .username.ng-dirty.ng-invalid-minlength {
            background-color: yellow;
        }

        .email.ng-valid {
            background-color: lightgreen;
        }
        .email.ng-dirty.ng-invalid-required {
            background-color: red;
        }
        .email.ng-dirty.ng-invalid-email {
            background-color: yellow;
        }

    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body  class="ng-cloak">
<div class="generic-container" ng-controller="CabController as ctrl">
    <div class="panel panel-default">
        <div class="panel-heading"><span class="lead">Cab Registration Form </span></div>
        <div class="formcontainer">
            <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                <input type="hidden" ng-model="ctrl.cab.id" />


                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="driver">Driver</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.cab.driver" id="driver" class="form-control input-sm" placeholder="Enter the driver's name"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="rating">Rating</label>
                        <div class="col-md-7">
                            <input type="number" ng-model="ctrl.cab.rating" id="rating" class="form-control input-sm" placeholder="Enter your rating."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-lable" for="location">Location</label>
                        <div class="col-md-7">
                            <input type="text" ng-model="ctrl.cab.location" id="location" class="form-control input-sm" placeholder="Enter your location."/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-actions floatRight">
                        <input type="submit"  value="{{!ctrl.user.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                        <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-default">
        <!-- Default panel contents -->
        <div class="panel-heading"><span class="lead">List of Users </span></div>
        <div class="tablecontainer">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Driver</th>
                    <th>Rating</th>
                    <th>Location</th>
                    <th width="20%"></th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="u in ctrl.cabs">
                    <td><span ng-bind="u.id"></span></td>
                    <td><span ng-bind="u.driver"></span></td>
                    <td><span ng-bind="u.rating"></span></td>
                    <td><span ng-bind="u.location"></span></td>
                    <td>
                        <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>
                        <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="<c:url value='/resources/core/js/app.js' />"></script>
<script src="<c:url value='/resources/core/js/service/cab_service.js' />"></script>
<script src="<c:url value='/resources/core/js/controller/cab_controller.js' />"></script>
</body>
</html>